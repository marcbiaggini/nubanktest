# Nubank Android Test

Inside of this document it will be found the necessary information for this project.

## Starting up

in this [link](https://drive.google.com/file/d/0B9GWZuZAoiKPZGYxbVJ1NmltLTQ/view) it will be founded all requirements related with this test.

## Dependency management

This project uses Gradle(currently used version `2.1.2`) within [AndroidStudio 2.1.2](http://tools.android.com/download/studio/builds/android-studio-2-1-2).

## Android Target

This test project will run only in Android api 23 or above and devices with Android 6.X.X or above.

## Architecture Description

This test project has been divided in two modules.

0. apilibrary: This module contains all related with endpoints interfaces.
0. app: This module contains all related with the application itself.


## Third Party Libraries Used
  
### apilibrary Module
0. [Project Lombok](https://projectlombok.org/) is mandatory [install](https://projectlombok.org/setup/android.html) this plug-in inside AndroidStudio to run the project
0. [Android Annotations](http://androidannotations.org/)
0. [Spring Framework for Android](http://projects.spring.io/spring-android/)
0. [Jackson Core](https://github.com/FasterXML/jackson-core)

### app Module
0. [Android Annotations](http://androidannotations.org/)
0. [Material Animated Switch](https://github.com/glomadrian/material-animated-switch)
0. [Uber Progress View](https://github.com/ishan1604/uberprogressview)
0. [Simple Arc Loader](https://github.com/generic-leo/SimpleArcLoader)

#Thank You!