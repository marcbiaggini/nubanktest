package amaro.apilibrary.ServiceInterfaces;

import org.androidannotations.rest.spring.annotations.Body;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import amaro.apilibrary.Model.CardResponse;
import amaro.apilibrary.Model.ChargeBack;
import amaro.apilibrary.Model.ChargebackNotify;
import amaro.apilibrary.Model.Notice;
import lombok.experimental.NonFinal;

/**
 * Created by juan.villa on 20/06/2016.
 */
@Rest(rootUrl = "https://nu-mobile-hiring.herokuapp.com", converters = {StringHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class, FormHttpMessageConverter.class})
public interface Endpoint extends RestClientErrorHandling {
  @Get("/notice")
  Notice getNotice();

  @Get("/chargeback")
  ChargeBack getChargeBack();

  @Post("/chargeback")
  CardResponse submmitChargeBack(@Body ChargebackNotify chargebackNotify);

  @Post("/card_block")
  CardResponse blockCard();

  @Post("/card_unblock")
  CardResponse unBlockCard();
}
