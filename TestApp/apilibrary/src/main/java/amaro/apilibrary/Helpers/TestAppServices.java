package amaro.apilibrary.Helpers;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import amaro.apilibrary.Error.RSRestErrorHandler;
import amaro.apilibrary.ServiceInterfaces.Endpoint;
import amaro.apilibrary.ServiceInterfaces.Endpoint_;

/**
 * Created by juan.villa on 20/06/2016.
 */
public class TestAppServices {
  public static Endpoint endpoint = null;

  public static void initializeServices(Context context, RSRestErrorHandler errorHandler) {
    endpoint = new Endpoint_(context);
  }

  public static Endpoint getEndpoint() {
    return endpoint;
  }

  public static boolean isOnline(Activity activity) {
    ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    return netInfo != null && netInfo.isConnectedOrConnecting();
  }
}
