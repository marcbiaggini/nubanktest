package amaro.apilibrary.Helpers;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

/**
 * Created by juan villa.
 */
public final class BusProvider extends Bus {

  private static BusProvider mInstance;
  private final Handler mainThread = new Handler(Looper.getMainLooper());

  public static synchronized BusProvider getInstance() {
    if (mInstance == null) {
      mInstance = new BusProvider();
    }
    return mInstance;
  }

  @Override
  public void post(final Object event) {
    if (Looper.myLooper() == Looper.getMainLooper()) {
      super.post(event);
    } else {
      mainThread.post(new Runnable() {
        @Override
        public void run() {
          BusProvider.super.post(event);
        }
      });

    }
  }

}
