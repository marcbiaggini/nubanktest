package amaro.apilibrary.Model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.Wither;

/**
 * Created by juan.villa on 21/06/2016.
 */
public class ChargebackNotify {

  private String comment;
  private List<MultiValueMap<String, Object>> reason_details = new ArrayList<>();

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public List<MultiValueMap<String, Object>> getReason_details() {
    return reason_details;
  }

  public void setReason_details(List<MultiValueMap<String, Object>> reason_details) {
    this.reason_details = reason_details;
  }
}
