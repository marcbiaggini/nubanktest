package amaro.api.ServiceInterfaces;

import android.util.Log;

import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Post;
import org.androidannotations.rest.spring.annotations.Rest;

import org.androidannotations.rest.spring.api.RestClientErrorHandling;
import org.androidannotations.rest.spring.api.RestErrorHandler;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import amaro.api.BuildConfig;
import amaro.api.Model.ChargeBack;
import amaro.api.Model.Notice;
import lombok.experimental.NonFinal;

/**
 * Created by juan.villa on 20/06/2016.
 */
@Rest(rootUrl ="https://nu-mobile-hiring.herokuapp.com", converters = {StringHttpMessageConverter.class, MappingJackson2HttpMessageConverter.class , FormHttpMessageConverter.class})
@NonFinal
public interface Endpoint extends RestClientErrorHandling {
  @Get("/notice")
  Notice getNotice();

  @Get("/chargeback")
  ChargeBack getChargeBack();

  @Post("/card_block")
  String blockCard();

  @Post("/card_unblock")
  String unBlockCard();
}
