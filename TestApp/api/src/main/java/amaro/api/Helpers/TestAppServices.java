package amaro.api.Helpers;

import android.content.Context;

import amaro.api.Error.RSRestErrorHandler;
import amaro.api.ServiceInterfaces.Endpoint;

/**
 * Created by juan.villa on 20/06/2016.
 */
public class TestAppServices {
  public static Endpoint endpoint = null;

  public static void initializeServices(Context context, RSRestErrorHandler errorHandler) {
    endpoint = new EndPoint_(context);
  }

  public static Endpoint getEndpoint() {
    return endpoint;
  }
}
