package amaro.testapp;

import android.app.Application;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EApplication;

import amaro.apilibrary.Error.RSRestErrorHandler;
import amaro.apilibrary.Helpers.TestAppServices;

/**
 * Created by juan.villa on 20/06/2016.
 */
@EApplication
public class TestApplication extends Application {
  @Bean
  RSRestErrorHandler errorHandler;

  @Override
  public void onCreate() {
    super.onCreate();
    TestAppServices.initializeServices(this, errorHandler);
  }
}
