package amaro.testapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.lazysource.uberprogressview.UberProgressView;

import amaro.apilibrary.Helpers.TestAppServices;
import amaro.apilibrary.Model.Notice;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {

  @ViewById(R.id.toolbar)
  Toolbar toolbar;
  @ViewById(R.id.description)
  TextView description;
  @ViewById(R.id.title)
  TextView title;
  @ViewById(R.id.continuar)
  Button continuar;
  @ViewById(R.id.fechar)
  Button fechar;
  @ViewById(R.id.errorMessageTextView)
  TextView errorMessageTextView;
  @ViewById(R.id.progressBar)
  UberProgressView progressBar;

  @AfterViews
  public void init() {
    progressBar.setVisibility(View.VISIBLE);
    setSupportActionBar(toolbar);
    if (TestAppServices.isOnline(this)) {
      getNotice();
    } else {
      errorMessageTextView.setVisibility(View.VISIBLE);
      progressBar.setVisibility(View.INVISIBLE);
    }
  }

  @Background
  public void getNotice() {
    try {
      Notice notice = TestAppServices.getEndpoint().getNotice();
      getNoticeThread(notice);
    } catch (Exception e) {
      errorMessageTextView.setText(getResources().getText(R.string.error_message));
      errorMessageTextView.setVisibility(View.VISIBLE);
      errorMessageTextView.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
      progressBar.setVisibility(View.INVISIBLE);
    }
  }

  @UiThread
  public void getNoticeThread(Notice notice) {
    title.setText(Html.fromHtml(notice.getTitle()));
    description.setText(Html.fromHtml(notice.getDescription()));
    continuar.setText(notice.getPrimaryAction().getTitle());
    fechar.setText(notice.getSecondaryAction().getTitle());
    continuar.setVisibility(View.VISIBLE);
    fechar.setVisibility(View.VISIBLE);
    title.setVisibility(View.VISIBLE);
    description.setVisibility(View.VISIBLE);
    errorMessageTextView.setVisibility(View.INVISIBLE);
    progressBar.setVisibility(View.INVISIBLE);
  }

  @Click(R.id.errorMessageTextView)
  public void tryAgain() {
    progressBar.setVisibility(View.VISIBLE);
    errorMessageTextView.setText(getResources().getText(R.string.loading));
    errorMessageTextView.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
    if (TestAppServices.isOnline(this)) {
      getNotice();
    } else {
      errorMessageTextView.setText(getResources().getText(R.string.error_message));
      errorMessageTextView.setVisibility(View.VISIBLE);
      errorMessageTextView.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
      progressBar.setVisibility(View.INVISIBLE);
    }
  }

  @Click(R.id.continuar)
  public void continuar() {
    ChargeBackActivity_.intent(getApplicationContext()).flags(Intent.FLAG_ACTIVITY_NEW_TASK).start();
  }

  @Click(R.id.fechar)
  public void fechar() {
    AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

    alertDialogBuilder.setTitle("Atenção!");
    alertDialogBuilder
        .setMessage(getResources().getString(R.string.finish_message_popUp))
        .setCancelable(false)
        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            finish();
          }
        })
        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
  }

}
