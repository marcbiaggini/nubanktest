package amaro.testapp.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;

import amaro.testapp.R;


/**
 * Created by juan.villa on 21/06/2016.
 */
public class SuccessMessageDialogFragment extends android.support.v4.app.DialogFragment {

  Button closeButton;
  private Dialog dialog;
  private CloseInterface mListener;
  public boolean isClosed =false;

  @Override
  public Dialog onCreateDialog(Bundle savedInstanceState) {
    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
    LayoutInflater inflater = getActivity().getLayoutInflater();
    View view = inflater.inflate(R.layout.fragment_sucess_message, null);

    closeButton = (Button) view.findViewById(R.id.dialogCloseButton);

    closeButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        isClosed = true;
        mListener.onClose(isClosed);
        dialog.dismiss();
      }
    });
    builder.setView(view);
    dialog = builder.create();
    dialog.show();

    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
    lp.copyFrom(dialog.getWindow().getAttributes());

    WindowManager wm = (WindowManager) dialog.getContext().getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    Point size = new Point();
    display.getSize(size);
    int height = size.y / 2;

    dialog.getWindow().setLayout(lp.width, height);
    return dialog;
  }

  @Override
  public void onAttach(Activity activity) {
    mListener = (CloseInterface) activity;
    super.onAttach(activity);
  }

  @Override
  public void onDetach() {
    mListener = null;
    super.onDetach();
  }

  public interface CloseInterface {
     void onClose(Boolean isClosed);
  }

}