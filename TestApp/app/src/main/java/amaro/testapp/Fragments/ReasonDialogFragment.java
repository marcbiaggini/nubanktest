package amaro.testapp.Fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import amaro.testapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ReasonDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReasonDialogFragment extends android.support.v4.app.DialogFragment implements TextView.OnEditorActionListener {
  // TODO: Rename parameter arguments, choose names that match
  // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

  private EditText mEditText;

  public ReasonDialogFragment() {
    // Required empty public constructor
  }

  /**
   * Use this factory method to create a new instance of
   * this fragment using the provided parameters.
   *
   * @return A new instance of fragment ReasonDialogFragment.
   */
  // TODO: Rename and change types and number of parameters
  public static ReasonDialogFragment newInstance() {
    ReasonDialogFragment fragment = new ReasonDialogFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    return inflater.inflate(R.layout.fragment_message, container, false);

  }

  @Override
  public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
    if (EditorInfo.IME_ACTION_DONE == actionId) {
      // Return input text back to activity through the implemented listener
      MessageDialogListener listener = (MessageDialogListener) getActivity();
      if (mEditText.getText().toString() != null && !mEditText.getText().toString().isEmpty()) {
        listener.onFinishEditDialog(mEditText.getText().toString());
        // Close the dialog and return back to the parent activity
        dismiss();
        return true;
      }

    }
    return false;
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);
    mEditText = (EditText) view.findViewById(R.id.etxReason);
    mEditText.setOnEditorActionListener(this);
    getDialog().getWindow().setSoftInputMode(
        WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    getDialog().setCancelable(false);
    getDialog().setCanceledOnTouchOutside(false);
  }

  public interface MessageDialogListener {
    void onFinishEditDialog(String inputText);
  }
}
