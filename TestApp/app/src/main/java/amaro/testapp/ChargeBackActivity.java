package amaro.testapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.glomadrian.materialanimatedswitch.MaterialAnimatedSwitch;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcLoader;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import amaro.apilibrary.Helpers.TestAppServices;
import amaro.apilibrary.Model.CardResponse;
import amaro.apilibrary.Model.ChargeBack;
import amaro.apilibrary.Model.ChargebackNotify;
import amaro.apilibrary.Model.Details;
import amaro.testapp.Fragments.ReasonDialogFragment;
import amaro.testapp.Fragments.SuccessMessageDialogFragment;

@EActivity(R.layout.activity_charge_back)
public class ChargeBackActivity extends AppCompatActivity implements ReasonDialogFragment.MessageDialogListener,SuccessMessageDialogFragment.CloseInterface {

  @ViewById(R.id.toolbar)
  Toolbar toolbar;
  @ViewById(R.id.lockIcon)
  ImageView lockIcon;
  @ViewById(R.id.blockChargeBack)
  TextView blockChargeBack;
  @ViewById(R.id.titleChargeBack)
  TextView titleChargeBack;
  @ViewById(R.id.option1)
  TextView option1;
  @ViewById(R.id.option2)
  TextView option2;
  @ViewById(R.id.footerChargeBack)
  TextView footerChargeBack;
  @ViewById(R.id.loadingMessage)
  TextView loadingMessage;
  @ViewById(R.id.loader)
  SimpleArcLoader loader;
  @ViewById(R.id.loaderSendChargeBack)
  SimpleArcLoader loaderSendChargeBack;
  @ViewById(R.id.switchOp1)
  MaterialAnimatedSwitch switchOp1;
  @ViewById(R.id.switchOp2)
  MaterialAnimatedSwitch switchOp2;
  @ViewById(R.id.contestar)
  Button contestar;
  @ViewById(R.id.cancelar)
  Button cancelar;

  String reason;
  MultiValueMap<String, Object> option1Map = new LinkedMultiValueMap<String, Object>();
  MultiValueMap<String, Object> option2Map = new LinkedMultiValueMap<String, Object>();
  List<MultiValueMap<String, Object>> reasonDetails = new ArrayList<>();
  boolean isLocked = true;
  SuccessMessageDialogFragment successMessageFragment = new SuccessMessageDialogFragment();


  @Override
  public void onFinishEditDialog(String inputText) {
    reason = inputText;
    reasonDetails.add(option1Map);
    reasonDetails.add(option2Map);
    loaderSendChargeBack.setVisibility(View.VISIBLE);
    loadingMessage.setText(getResources().getText(R.string.sending_charge));
    loadingMessage.setVisibility(View.VISIBLE);
    contestar.setClickable(false);
    cancelar.setClickable(false);
    sendChargebackThread(reason, reasonDetails);
  }

  @Override
  public void onClose(Boolean isClosed) {
    if(isClosed){
      finish();
    }
  }

  @AfterViews
  public void init() {
    setSupportActionBar(toolbar);
    int[] colors = {R.color.closegray, R.color.disablegray};
    ArcConfiguration configuration = new ArcConfiguration(this);
    configuration.setColors(colors);
    loaderSendChargeBack.refreshArcLoaderDrawable(configuration);
    switchListeners();
    loaderSendChargeBack.setVisibility(View.VISIBLE);
    loadingMessage.setText(getResources().getText(R.string.loading_charge));
    if (TestAppServices.isOnline(this)) {
      getChargebackThread();
    } else {
      errorDialog("chargeback");
    }
  }

  @Background
  public void getChargebackThread() {
    try {
      setChargeBack(TestAppServices.endpoint.getChargeBack());
    } catch (Exception e) {
      errorDialog("chargeback");
    }
  }

  @Background
  public void blockCardThread() {
    try {
      CardResponse cardResponse = TestAppServices.getEndpoint().blockCard();
      if (cardResponse.getStatus().equalsIgnoreCase("ok")) {
        setLockedIcon();
      }
    } catch (Exception e) {
      lockIcon.setClickable(true);
      errorDialog("card");
    }
  }

  @Background
  public void unBlockCardThread() {
    try {
      CardResponse cardResponse = TestAppServices.getEndpoint().unBlockCard();
      if (cardResponse.getStatus().equalsIgnoreCase("ok")) {
        setUnlockedIcon();
      }
    } catch (Exception e) {
      lockIcon.setClickable(true);
      errorDialog("card");
    }
  }

  @Background
  public void sendChargebackThread(String reason, List<MultiValueMap<String, Object>> reasonDetails) {
    try {
      ChargebackNotify chargebackNotify = new ChargebackNotify();
      chargebackNotify.setComment(reason);
      chargebackNotify.setReason_details(reasonDetails);
      if (TestAppServices.getEndpoint().submmitChargeBack(chargebackNotify).getStatus().equalsIgnoreCase("ok")) {
        sendChargeBackResponse();
      } else {
        errorDialog("sendChargeBackResponse");
      }
    } catch (Exception e) {
      errorDialog("sendChargeBackResponse");
    }
  }

  @UiThread
  public void setChargeBack(ChargeBack chargeBack) {
    titleChargeBack.setText(chargeBack.getTitle());
    footerChargeBack.setText(Html.fromHtml(chargeBack.getComment_hint()));
    loaderSendChargeBack.setVisibility(View.INVISIBLE);
    loadingMessage.setVisibility(View.INVISIBLE);
    for (Iterator<Details> iterator = chargeBack.getReason_details().iterator(); iterator.hasNext(); ) {
      Details details = iterator.next();
      if (details.getId().equalsIgnoreCase("merchant_recognized")) {
        option1.setText(details.getTitle());
        option1Map.add("id",details.getTitle());
        option1Map.add("response", false);
      } else if (details.getId().equalsIgnoreCase("card_in_possession")) {
        option2Map.add("id",details.getTitle());
        option2Map.add("response", false);
        option2.setText(details.getTitle());
      }
    }
  }

  @UiThread
  public void setLockedIcon() {
    lockIcon.setImageDrawable(getDrawable(R.mipmap.ic_chargeback_lock));
    lockIcon.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
    blockChargeBack.setText(getResources().getText(R.string.lock_charge_back));
    blockChargeBack.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
    isLocked = true;
    lockIcon.setClickable(true);
    loader.setVisibility(View.INVISIBLE);
  }

  @UiThread
  public void setUnlockedIcon() {
    lockIcon.setImageDrawable(getDrawable(R.mipmap.ic_chargeback_unlock));
    lockIcon.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
    blockChargeBack.setText(getResources().getText(R.string.unlock_charge_back));
    blockChargeBack.startAnimation(AnimationUtils.loadAnimation(getBaseContext(), R.anim.fade_in_dialog));
    isLocked = false;
    lockIcon.setClickable(true);
    loader.setVisibility(View.INVISIBLE);
  }

  @UiThread
  public void errorDialog(final String action) {
    AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

    alertDialogBuilder.setTitle("Error");
    alertDialogBuilder
        .setMessage(getResources().getString(R.string.error_message_popUp))
        .setCancelable(false)
        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            switch (action) {
              case "card":
                lockCard();
                break;
              case "chargeback":
                getChargebackThread();
                break;
              default:
                loaderSendChargeBack.setVisibility(View.VISIBLE);
                sendChargebackThread(reason, reasonDetails);
            }
          }
        })
        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
            finish();
          }
        });
    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
  }

  @UiThread
  public void sendChargeBackResponse() {
    loaderSendChargeBack.setVisibility(View.INVISIBLE);
    loadingMessage.setVisibility(View.INVISIBLE);
    loadingMessage.setVisibility(View.INVISIBLE);
    contestar.setClickable(true);
    cancelar.setClickable(true);
    showSuccessDialog();
  }

  @Click(R.id.lockIcon)
  public void lockCard() {
    lockIcon.setClickable(false);
    loader.setVisibility(View.VISIBLE);

    if (isLocked) {
      blockChargeBack.setText(getResources().getText(R.string.unlocking));
      unBlockCardThread();
    } else {
      blockChargeBack.setText(getResources().getText(R.string.locking));
      blockCardThread();
    }
  }

  @Click(R.id.contestar)
  public void contestar() {
    showReasonDialog();
  }

  @Click(R.id.cancelar)
  public void cancelar(){
    AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);

    alertDialogBuilder.setTitle("Atenção!");
    alertDialogBuilder
        .setMessage(getResources().getString(R.string.cancel_message_popUp))
        .setCancelable(false)
        .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            finish();
          }
        })
        .setNegativeButton("Não", new DialogInterface.OnClickListener() {
          public void onClick(DialogInterface dialog, int id) {
            dialog.cancel();
          }
        });
    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
  }

  private void showReasonDialog() {
    FragmentManager fm = getSupportFragmentManager();
    ReasonDialogFragment messageFragment = ReasonDialogFragment.newInstance();
    messageFragment.show(fm, "fragment_message");
  }

  private void showSuccessDialog() {
    FragmentManager fm = getSupportFragmentManager();
    successMessageFragment.show(fm,"fragment_sucess_message");
  }

  private void switchListeners() {
    switchOp1.setOnCheckedChangeListener(new MaterialAnimatedSwitch.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(boolean state) {
        if (state) {
          /*switch is set to right*/
          option1Map.clear();
          option1Map.add("id", option1.getText().toString());
          option1Map.add("response", true);
        } else {
          /* switch is set to left */
          option1Map.clear();
          option1Map.add("id", option1.getText().toString());
          option1Map.add("response", false);
        }
      }
    });

    switchOp2.setOnCheckedChangeListener(new MaterialAnimatedSwitch.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(boolean state) {
        if (state) {
          /*switch is set to right*/
          option2Map.clear();
          option2Map.add("id", option2.getText().toString());
          option2Map.add("response", true);
        } else {
          /* switch is set to left */
          option2Map.clear();
          option2Map.add("id", option2.getText().toString());
          option2Map.add("response", false);
        }
      }
    });
  }

}
